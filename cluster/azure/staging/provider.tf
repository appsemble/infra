# Declare required providers
terraform {
  required_providers {
    # Terraform provider
    # Link: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }

    # Terraform provider for Ansible
    # Link: https://registry.terraform.io/providers/ansible/ansible/latest/docs
    ansible = {
      version = "=1.3.0"
      source  = "ansible/ansible"
    }
  }
  required_version = ">= 0.14"
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}

  client_id       = var.azure_client_id
  client_secret   = var.azure_client_secret
  tenant_id       = var.azure_tenant_id
  subscription_id = var.azure_subscription_id
}
