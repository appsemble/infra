# All outputs for the terraform configuration

# Output the main control plane node IP address
output "main_control_plane" {
  value       = data.azurerm_public_ip.main_control_plane_ip_data.ip_address
  description = "The public IP address of the main control plane node."
}

# Output the worker nodes IP addresses
# output "worker_node" {
#   value       = data.azurerm_public_ip.worker_node_ip_data.*.ip_address
#   description = "The public IP addresses of the worker nodes."
# }

# Output the Load Balancer IP address
output "lb" {
  value       = data.azurerm_public_ip.lb_ip_data.ip_address
  description = "The public IP addresses of the load balancer"
}
