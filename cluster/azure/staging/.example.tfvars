# Take missing environment variables from Bitwarden, vault called "appsemble-cluster-azure-staging"

#
# General
#
azure_client_id = "<Client ID>"
azure_client_secret = "<Client Secret>"
azure_tenant_id = "<Tenant ID>"
azure_subscription_id = "<Subscription ID>"

# Data center location, includes NL
location = "West Europe"

#
# Resource group
#
azure_resource_gr_name = "appsemble-staging-resource-gr"

#
# Availability set
#
azure_availability_set_name = "availability-set"
azure_availability_set_environment = "staging"

#
# SSH key
#
ssh_key_name = "ssh"
ssh_pvt_key = ".ssh/id_rsa"
ssh_pub_key = ".ssh/id_rsa.pub"

#
# Network
#
network_name = "network"
network_ip_range = "10.0.0.0/16"
network_subnet_name = "network-subnet"
network_subnet_range = "10.0.0.0/16"
azure_network_security_group_name = "nsg"

#
# Load Balancer
#
azure_lb_ip_name = "lb-ip"
azure_lb_ipconfig_name = "lb-ipconfig"
azure_lb_priv_ip = "10.0.0.254"
azure_lb_name = "lb"
azure_lb_backend_address_pool_name = "lb-backend-address-pool"

#
# Server/OS general
#
azure_admin_username = "admin-user"
azure_os_publisher = "Canonical"
azure_os_offer = "ubuntu-24_04-lts"
azure_os_sku = "server"
azure_os_version = "latest"

#
# Control plane
#
control_plane_name_prefix = "control-plane"
control_plane_server_type = "Standard_B2s"
control_plane_ip_name_prefix = "control-plane-ip"
control_plane_ipconfig_name_prefix = "control-plane-ipconfig"
control_plane_nic_name_prefix = "control-plane-nic"
control_plane_disk_size = "64"
# Starting from .4 as in Azure the first fours IPs are reserved
main_node_priv_ip = "10.0.0.4"

#
# Worker nodes
#
worker_name_prefix = "worker"
worker_server_type = "Standard_B2s"
worker_ip_name_prefix = "worker-ip"
worker_ipconfig_name_prefix = "worker-ipconfig"
worker_nic_name_prefix = "worker-nic"
# 10.0.0.4, .5, .6 are reserved for control plane nodes private IP addresses
# Thus, the numbering starts from .7
worker_priv_start_index = 7
# Worker disk size in GB
worker_disk_size = "64"

#
# Cluster general 
#
k3s_token_secret = "<k3s-token-secret>"
