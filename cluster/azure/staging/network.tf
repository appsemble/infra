# Network-related resources

# Virtual network
resource "azurerm_virtual_network" "network" {
  name                = var.network_name
  address_space       = [var.network_ip_range]
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name
}

# Network subnet
resource "azurerm_subnet" "network_subnet" {
  name                 = var.network_subnet_name
  resource_group_name  = azurerm_resource_group.resource_gr.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [var.network_subnet_range]

  depends_on = [azurerm_resource_group.resource_gr]
}

# Network security group
resource "azurerm_network_security_group" "nsg" {
  name                = var.azure_network_security_group_name
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name

  # Allow SSH
  security_rule {
    name                       = "SSH-in"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefixes    = local.allowed_source_ips
    destination_address_prefix = "*"
  }

  # Allow HTTPs in
  security_rule {
    name                       = "HTTPS-in"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes    = local.allowed_source_ips
    destination_address_prefix = "*"
  }

  # Allow cluster access 6443
  security_rule {
    name                       = "ClusterAccess-in"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "6443"
    source_address_prefixes    = local.allowed_source_ips
    destination_address_prefix = "*"
  }

  # Allow HTTPs out
  security_rule {
    name                       = "HTTPS-out"
    priority                   = 1001
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Associate the Network Security Group to the subnet
resource "azurerm_subnet_network_security_group_association" "subnet_nsg_association" {
  subnet_id                 = azurerm_subnet.network_subnet.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
