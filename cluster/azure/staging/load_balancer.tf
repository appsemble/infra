# Load Balancer

# Public IP for the load balancer
resource "azurerm_public_ip" "lb_ip" {
  name                = var.azure_lb_ip_name
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name
  allocation_method   = "Static"
  sku                 = "Standard" # Need to match sku of the load balancer

  # Public IP should not be deleted
  lifecycle {
    prevent_destroy = true
  }
}

# Load Balancer
resource "azurerm_lb" "lb" {
  name                = var.azure_lb_name
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = var.azure_lb_ipconfig_name
    public_ip_address_id = azurerm_public_ip.lb_ip.id
    private_ip_address   = var.azure_lb_priv_ip
  }
}

# Backend address pool
resource "azurerm_lb_backend_address_pool" "lb_backend_address_pool" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = var.azure_lb_backend_address_pool_name
}

# Data resource to get the load balancer public IP
data "azurerm_public_ip" "lb_ip_data" {
  name                = azurerm_public_ip.lb_ip.name
  resource_group_name = azurerm_lb.lb.resource_group_name
}

# Load Balancer probe for cluster access
resource "azurerm_lb_probe" "lb_probe_cluster_access" {
  protocol            = "Tcp"
  loadbalancer_id     = azurerm_lb.lb.id
  name                = "tcp-cluster-access-probe"
  port                = 6443
  interval_in_seconds = 10
  number_of_probes    = 3
}

# Load Balancer rule for cluster access
resource "azurerm_lb_rule" "lb_rule_cluster_access" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "lb-rule-cluster-access"
  protocol                       = "Tcp"
  frontend_port                  = 6443
  backend_port                   = 6443
  frontend_ip_configuration_name = var.azure_lb_ipconfig_name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.lb_backend_address_pool.id]
  probe_id                       = azurerm_lb_probe.lb_probe_cluster_access.id
}

# Load Balancer probe for HTTPS forwarding
resource "azurerm_lb_probe" "lb_probe_https_forwarding" {
  protocol            = "Tcp"
  loadbalancer_id     = azurerm_lb.lb.id
  name                = "tcp-https-forwarding-probe"
  port                = 443
  interval_in_seconds = 10
  number_of_probes    = 3
}

# Load Balancer rule for HTTPS forwarding
resource "azurerm_lb_rule" "lb_rule_https_forwarding" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "lb-rule-https_forwarding"
  protocol                       = "Tcp"
  frontend_port                  = 443
  backend_port                   = 443
  frontend_ip_configuration_name = var.azure_lb_ipconfig_name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.lb_backend_address_pool.id]
  probe_id                       = azurerm_lb_probe.lb_probe_https_forwarding.id
}

# Load Balancer probe for HTTP forwarding
# TODO: Remove once the environment is released
resource "azurerm_lb_probe" "lb_probe_http_forwarding" {
  protocol            = "Tcp"
  loadbalancer_id     = azurerm_lb.lb.id
  name                = "tcp-http-forwarding-probe"
  port                = 80
  interval_in_seconds = 10
  number_of_probes    = 3
}

# Load Balancer rule for HTTP forwarding
# TODO: Remove once the environment is released
resource "azurerm_lb_rule" "lb_rule_http_forwarding" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "lb-rule-http_forwarding"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = var.azure_lb_ipconfig_name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.lb_backend_address_pool.id]
  probe_id                       = azurerm_lb_probe.lb_probe_http_forwarding.id
}
