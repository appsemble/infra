# Availability set
resource "azurerm_availability_set" "availability_set" {
  name                = var.azure_availability_set_name
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name

  tags = {
    environment = var.azure_availability_set_environment
  }
  depends_on = [azurerm_resource_group.resource_gr]
}
