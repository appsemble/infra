# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/ansible/ansible" {
  version     = "1.3.0"
  constraints = "1.3.0"
  hashes = [
    "h1:Ki8m3lAW3l6f6tPktHiyecEzOydKACJv/vMriuLDWhE=",
    "zh:0307c80c6a890b629e866318688ac6c73acd99dfd61c371e9036feb001baff45",
    "zh:28bddf36dce285d79391b7bbceae08e33533da4412f9c9434eb769a1e75c3992",
    "zh:3979596203cc64fd602a1e03cca198c2071f85ec29bf8c8816b35cc74b271411",
    "zh:3bd68c5c9344e5de161688d49b286d25f32aaa9b47418ff4025db42b1a00fdf9",
    "zh:415c46bd16f57e54564366ebdfed3dc69b0a27ef6f02828dbdbd874dee11627a",
    "zh:4e7a47e95f8bb5e1d8b39be49e57101c820ae2a8a998fb9cc0ce56badea1b26a",
    "zh:59e29ff43ddd0afb8341b45b3456c457ef9bfd091e2e6b4dafa0aaa4c0aa1c75",
    "zh:7e3572a8654cd4e814077e3622bb9224b02c6fd59a9728049e22dc9d62bfdd1d",
    "zh:82d4b17b383a78e0ba5c0639c1d92edcc5ece8dc73bda5f9235c0f7f2330618b",
    "zh:8833120ae5bad12fb0b7bb6f50770b66ed9a8441c8c126f68969d45985a6d600",
    "zh:a9c042b23d24d5a10eabb012bdd5ca0b0766074019d2380f713fcbfaf7c55c31",
    "zh:b9756dd7aa0da71de765d9ec5faf03b26fe0d212af517cc8b453b17f673d64c0",
    "zh:c73c81d669b5be4f6cc900277a42b14b73dc96ebd7bbc518ccd7984c26761c65",
    "zh:e9e76b2d86a96f90168b06e411281cae5b2b01b260cd6211b9d67923e8414072",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.opentofu.org/hashicorp/azurerm" {
  version     = "3.0.0"
  constraints = "3.0.0"
  hashes = [
    "h1:1hU4p9waRHVrGnJWQ2Ef7Vrdmo8MbFDBdqqemi5R2+Q=",
    "zh:0318f1813960360a800048de23c0d7ad1f5649ebc949be4997b870392a738117",
    "zh:0550355c5baaf05c5858041bc1cf9314c67f4424504a4d3f72bc60a04defe437",
    "zh:17f5e7af0ec59f4b8ada46aa23423f37073106995fdee288abe446b9c4223c6a",
    "zh:33111e929e78d156357a891e21d2d3b2b46c11644f014a92cdfdecca1b0c6c77",
    "zh:3e73f062082e82d5c432982cc2c67aa640c1da8db99fbdc1f0ef51c9e3caa8bd",
    "zh:532162e390f537d6a8846e0a35f9e745e2f747382d2543b8e945d2ee72aea5f4",
    "zh:78aa5c101b1272ed6187f67cbf418dcb97397550421b04c7f9baec311c1e6f1e",
    "zh:c58f4b48482de835dc7bde5431495ac107d5bcbd5c68c0254e27e82632f6304c",
    "zh:f48eecb2704207d1fe900defb0ec923f5990a05684a3333720c6d50145f94f45",
    "zh:fdd5b764e45ece1447c256e0b6a0486cadc19bbbb6e0952499050eeae638243a",
  ]
}
