# Control plane

# Main control plane public IP
resource "azurerm_public_ip" "main_control_plane_ip" {
  name                    = "${var.control_plane_ip_name_prefix}-0"
  location                = azurerm_resource_group.resource_gr.location
  resource_group_name     = azurerm_resource_group.resource_gr.name
  allocation_method       = "Static"
  sku                     = "Standard"
  idle_timeout_in_minutes = 30

  # Public IP should not be deleted
  lifecycle {
    prevent_destroy = true
  }
}

# Main control plane network interface (NIC)
resource "azurerm_network_interface" "main_control_plane_nic" {
  name                = "${var.control_plane_nic_name_prefix}-0"
  location            = azurerm_resource_group.resource_gr.location
  resource_group_name = azurerm_resource_group.resource_gr.name

  ip_configuration {
    name                          = "${var.control_plane_ipconfig_name_prefix}-0"
    subnet_id                     = azurerm_subnet.network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.main_node_priv_ip
    public_ip_address_id          = azurerm_public_ip.main_control_plane_ip.id
  }
  depends_on = [azurerm_subnet_network_security_group_association.subnet_nsg_association]
}

# Associate Main control plane NIC to the backend address pool for the load balancer
resource "azurerm_network_interface_backend_address_pool_association" "main_control_plane_nic_backend_address_pool" {
  network_interface_id    = azurerm_network_interface.main_control_plane_nic.id
  ip_configuration_name   = "${var.control_plane_ipconfig_name_prefix}-0"
  backend_address_pool_id = azurerm_lb_backend_address_pool.lb_backend_address_pool.id
}

# Main control plane node
resource "azurerm_linux_virtual_machine" "main_control_plane" {
  name                = "${var.control_plane_name_prefix}-0"         # The name of the virtual machine
  resource_group_name = azurerm_resource_group.resource_gr.name      # The name of the resource group in which the Linux VM should exist
  location            = azurerm_resource_group.resource_gr.location  # The location where the Linux VM should exist
  availability_set_id = azurerm_availability_set.availability_set.id # The availability set ID to which the VM should belong
  size                = var.control_plane_server_type                # The VM type
  admin_username      = var.azure_admin_username                     # The username of the local administrator
  # Network interface IDs to be attached to this VM
  network_interface_ids = [
    azurerm_network_interface.main_control_plane_nic.id,
  ]

  admin_ssh_key {
    username   = var.azure_admin_username
    public_key = file(var.ssh_pub_key)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = var.control_plane_disk_size
  }

  source_image_reference {
    publisher = var.azure_os_publisher
    offer     = var.azure_os_offer
    sku       = var.azure_os_sku
    version   = var.azure_os_version
  }
}

data "azurerm_public_ip" "main_control_plane_ip_data" {
  name                = azurerm_public_ip.main_control_plane_ip.name
  resource_group_name = azurerm_linux_virtual_machine.main_control_plane.resource_group_name
}

resource "ansible_playbook" "main_control_plane_config" {
  playbook   = "../../../config/config-main-control-plane.yml"
  name       = data.azurerm_public_ip.main_control_plane_ip_data.ip_address
  replayable = true

  extra_vars = {
    inventory_hostname           = data.azurerm_public_ip.main_control_plane_ip_data.ip_address
    ansible_user                 = var.azure_admin_username
    ansible_ssh_private_key_file = var.ssh_pvt_key
    ansible_python_interpreter   = "/usr/bin/python3"
    k3s_token_secret             = var.k3s_token_secret
    tls_san_ip                   = data.azurerm_public_ip.lb_ip_data.ip_address
  }

  depends_on = [azurerm_linux_virtual_machine.main_control_plane]
}
