# Cluster setup

This sets up a cluster on Azure

## Variables

### Generating SSH keys for accessing the cluster with Ansible

```sh
ssh-keygen -t rsa -b 4096 -C "info@appsemble.com"
# required for the keys to work
chmod 600 .ssh/id_rsa .ssh/id_rsa.pub
```

### Generating `k3s_token_secret`

```sh
# if placeholder value in .tfvars, generate a new secret from random bytes in hex and write it to .tfvars
[ grep -q 'k3s_token_secret *= *"<k3s-token-secret>"' .tfvars ] &&
head -c 64 /dev/random | xxd -p | tr -d '\n' |
xargs -I{} sed -i 's/^ *k3s_token_secret *=.*/k3s_token_secret = "{}"' .tfvars
```

### Necessary Azure variables

These are the most important variables we need to change from the `.example.tfvars`.

```terraform-vars
azure_client_id = "<Client ID>"
azure_client_secret = "<Client Secret>"
azure_tenant_id = "<Tenant ID>"
azure_subscription_id = "<Subscription ID>"
```
To find **`azure_client_id`**: "Subscriptions" -> "Microsoft Azure Sponsorship" -> you should see it on the dashboard

Then, run the following commands:
- `az login`
- `az account set --subscription=<YOUR SUBSCRIPTION ID>`
- `az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/<YOUR SUBSCRIPTION ID>"`
	- This will output 5 variables we need in JSON:
		- `appId` - `azure_client_id`
		- `password` - `azure_client_secret`
		- `tenant` - `azure_tenant_id`

### Office and internet.nl IP address/range

For the office range, figure it out:

```
curl https://icanhazip.com
# curl -6 https://icanhazip.com # No variables for IPv6 yet
```

The internet.nl range is as follows:

```
IPv4: 62.204.66.0/26
IPv6: 2a00:d00:ff:162::/64
```

### Working with existing cluster? Check BitWarden.

Secrets should be in the folder `appsemble-azure-staging`.
