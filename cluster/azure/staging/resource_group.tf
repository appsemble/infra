# Resource group
resource "azurerm_resource_group" "resource_gr" {
  name     = var.azure_resource_gr_name
  location = var.location
}
