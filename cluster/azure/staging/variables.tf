# All variables for the terraform configuration

#
# General
#
variable "azure_client_id" {
  type        = string
  description = "The client id for the Azure service principal"
  sensitive   = true
}

variable "azure_client_secret" {
  type        = string
  description = "The client secret for the Azure service principal"
  sensitive   = true
}

variable "azure_tenant_id" {
  type        = string
  description = "The tenant id for the Azure service principal"
  sensitive   = true
}

variable "azure_subscription_id" {
  type        = string
  description = "The Azure subscription id"
  sensitive   = true
}

variable "location" {
  type        = string
  description = "The data center location in which the resources will be created"
}

#
# Resource group
#
variable "azure_resource_gr_name" {
  type        = string
  description = "The name of the Azure resource group"
}

#
# Availability set
#
variable "azure_availability_set_name" {
  type        = string
  description = "The name of the Azure availability set"
}

variable "azure_availability_set_environment" {
  type        = string
  description = "The environment of the Azure availability set"
}

#
# SSH key
#
variable "ssh_key_name" {
  type        = string
  description = "The SSH key name to use for the VMs"
}

variable "ssh_pvt_key" {
  type        = string
  description = "The SSH private key file path"
}

variable "ssh_pub_key" {
  type        = string
  description = "The SSH public key file path"
}

#
# Network
#
variable "network_name" {
  type        = string
  description = "The network name"
}

variable "network_ip_range" {
  type        = string
  description = "The network IP range"
}

variable "network_subnet_name" {
  type        = string
  description = "The network subnet name"
}

variable "network_subnet_range" {
  type        = string
  description = "The network subnet IP range"
}

variable "azure_network_security_group_name" {
  type        = string
  description = "The network security group name"
}

variable "office_router_public_ip" {
  type        = string
  description = "The office router public IP"
}

variable "internet_nl_public_ip_range" {
  type        = string
  description = "The Internet.nl public IP range"
}

locals {
  # Allowed source IPs for the NSG rules in comma-separated string format
  allowed_source_ips = [
    var.office_router_public_ip,
    var.internet_nl_public_ip_range
  ]
}

#
# Load Balancer
#
variable "azure_lb_ip_name" {
  type        = string
  description = "The Load balancer IP name"
}

variable "azure_lb_ipconfig_name" {
  type        = string
  description = "The Load balancer IP configuration name"
}

variable "azure_lb_name" {
  type        = string
  description = "The Load balancer name"
}

variable "azure_lb_priv_ip" {
  type        = string
  description = "The Load balancer private IP address"
}

variable "azure_lb_backend_address_pool_name" {
  type        = string
  description = "The name of the Load balancer backend address pool"
}

#
# Server
#
variable "azure_admin_username" {
  type        = string
  description = "The admin username for the VM"
  sensitive   = true
}

variable "azure_os_publisher" {
  type        = string
  description = "The OS publisher for the VM"
}

variable "azure_os_offer" {
  type        = string
  description = "The OS offer for the VM"
}

variable "azure_os_sku" {
  type        = string
  description = "The OS sku for the VM"
}

variable "azure_os_version" {
  type        = string
  description = "The OS version for the VM"
}

#
# Control plane
#
variable "control_plane_ip_name_prefix" {
  type        = string
  description = "The control plane IP name prefix"
}

variable "control_plane_nic_name_prefix" {
  type        = string
  description = "The control plane network interface (NIC) name prefix"
}

variable "control_plane_ipconfig_name_prefix" {
  type        = string
  description = "The control plane IP configuration name prefix"
}

variable "control_plane_name_prefix" {
  type        = string
  description = "The name prefix for the control plane nodes"
}

variable "control_plane_server_type" {
  type        = string
  description = "The server type/size for the control plane nodes"
}

variable "control_plane_disk_size" {
  type        = string
  description = "The control plane node disk size"
}

variable "main_node_priv_ip" {
  type        = string
  description = "The private IP address for the main control plane node"
}

#
# Worker nodes
#
variable "worker_name_prefix" {
  type        = string
  description = "The worker node name prefix"
}

variable "worker_server_type" {
  type        = string
  description = "The worker node server type"
}

variable "worker_ip_name_prefix" {
  type        = string
  description = "The worker node ip name prefix"
}

variable "worker_ipconfig_name_prefix" {
  type        = string
  description = "The worker node IP configuration name prefix"
}

variable "worker_nic_name_prefix" {
  type        = string
  description = "The worker node network interface (NIC) name prefix"
}

variable "worker_priv_start_index" {
  type        = string
  description = "The worker node private IP address start index"
}

variable "worker_disk_size" {
  type        = string
  description = "The worker node disk size"
}

#
# Cluster general
#
variable "k3s_token_secret" {
  type        = string
  description = "The k3s token secret"
  sensitive   = true
}
