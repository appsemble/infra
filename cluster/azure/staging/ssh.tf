# SSH key
resource "azurerm_ssh_public_key" "ssh_key" {
  name                = var.ssh_key_name
  resource_group_name = azurerm_resource_group.resource_gr.name
  location            = var.location
  public_key          = file(var.ssh_pub_key)

  depends_on = [azurerm_resource_group.resource_gr]
}

