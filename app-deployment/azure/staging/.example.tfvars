appsemble_name = "appsemble"
appsemble_repo = "https://charts.appsemble.com"
appsemble_chart = "appsemble/appsemble"
appsemble_namespace = "appsemble"
appsemble_version = "0.29.4"

appsemble_ingress_host = "appsemble.nl"

appsemble_ingress_enabled = true
