# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/ansible/ansible" {
  version     = "1.2.0"
  constraints = "~> 1.2.0"
  hashes = [
    "h1:+FFI791dQLSgrP3ldRPckp6kKHKPByGlZnZHSKEIk9M=",
    "zh:06ec85f5c8809eebd7049846d3a45e5c4d052f021fd0a93681d72da8db18f03c",
    "zh:13a289fbc8ba46b4a81aae5548d0f4b7c772e60b5597493be1a2f36f1642252f",
    "zh:1e345d4dd6cdff194c5878a986d00494dfe4d51e5f05a85fa37243acada4ac98",
    "zh:3bfc27e99ae4d4ad073f89655e88d545a043f395581fe6d30f9d2ddea98de8bc",
    "zh:45798319359ac89c70fc474a8cf11888b7f4281e591fc41892a55449740aa170",
    "zh:578b8b3f58f2a368bbb1326f16a14a7ff4f701a00f1123deffd1de751f9c2e28",
    "zh:6160b0e5728e7fc905e1b17671fb0df74483c477d0262a6c1e51c89d48afe71f",
    "zh:98961ef4beb153d15742dafc1a0428bf45323e979f4b4a82a4ec4062205a0782",
    "zh:a26d76f427058ec529436bf8fbe96bf6cd4e0a29275d7a6add0d3357517e0d43",
    "zh:a70366e2e21ed51d26e7797b3679db3d812e3458e7980e906934e78599461f02",
    "zh:a9829a90cd302789dc277a1be55ddaff83e702159dd4a05646cedd17b5c337b9",
    "zh:beb105f90d9a9d7821f533bee8ccddc9daed7c3a59646e5b3777ba62253f84f1",
    "zh:bf8db622d0cb848bc843d29187b747388d73d520ee10814424fdf79693ea15d5",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f6c47554e1e98d71dc1ca5fbb569db198f71c13b4a1375454839b44e90a91926",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.13.1"
  constraints = "2.13.1"
  hashes = [
    "h1:RnGqyQnbJw8MkW5VSZKivvGxW0dOepOZ4BBWqC9IGmg=",
    "zh:4430984f356eeba398d738fb97fc04f4225ade7b1cf3fac1ef0d9fcd1275c6a5",
    "zh:48f89bdcca891c17f8af45b1b5df217feb2c77cfea76753aff7e91ddc9984e25",
    "zh:5ca80b3f4d793427e99f59575a5d2d03a428314275e280e054541417f62c5f43",
    "zh:6c9ed1717b64374cce020e0c8bf2a7492e504db200b748166cddefbcc04deb8e",
    "zh:963f25e98b803f9bb11ca843293c9ddb6d1e4f8615c9b545a7d9db37281c52e5",
    "zh:a15fd28f1a63f0547b4eabeafb753ccf993e39c184b6a645a1e3d47af46db48a",
    "zh:a1e83568329debbfce65140546d7030854f1110a2e7d3f1f9ce73b9d1e8da3c7",
    "zh:d858bf799c3ee27a16aad93445decf1aec153ab5a01c198025a54784601a770f",
    "zh:df2b997ce25c612e2f3a1c62600f06c33cf51139a917a9340a776c5e2b2c4379",
    "zh:e1485634a4ea8a768a66523308555cbf58dc258d1352793a1c90a1c67c242c2c",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.29.0"
  constraints = "2.29.0"
  hashes = [
    "h1:6wueJiWzDIQiMruGlxPZe8xtsyRs+IsNUb2TJ0P0nT8=",
    "zh:2467de940f98ef5d3ed977a0f6b797962cd9ae6210ef706b8f6e6db23a0b3b99",
    "zh:480a2ccc9e1f3a444b6ebf836d87061002be00c54482be7180e090dddc47809e",
    "zh:4ce04ba31734813d6636b51b2346b8262253264033be2775d66e8298551c2dde",
    "zh:56b94fcd5ba65cae892fd64e831838369ae4615582c314eee73fa2e513689991",
    "zh:5a7e858dc3600e542182abcec9079e2f8741d1ba72114e87668ef64679e7191a",
    "zh:905b6eb78f19bd80b22c688af06130353977f77f313738c8e0cfc524e8550d4c",
    "zh:ccf5c3e7383d11785a735a0ce7751e4ff394b133aa5085857139eaec1d9a54c1",
    "zh:eb4e72d3abf937c283f702342eb1fc820b3dbfc743b1e24c84c3604c8fca1988",
    "zh:eea59cd51ef366269231cbfa5b77fc3b9fbebecfd6bca8dff27e5abd96188d92",
    "zh:f026a8b4fa2ca566c3d6cd9bfdd0dd58c0631e3d945b98a8ced0943aa27dd4bd",
  ]
}
