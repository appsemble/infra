terraform {
  required_providers {
    # Terraform provider
    # Link: https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.29.0"
    }

    # Terraform provider
    # Link: https://registry.terraform.io/providers/hashicorp/helm/latest/docs
    helm = {
      source  = "hashicorp/helm"
      version = "2.13.1"
    }

    # Terraform provider for Ansible
    # Link: https://registry.terraform.io/providers/ansible/ansible/latest/docs
    ansible = {
      version = "~> 1.2.0"
      source  = "ansible/ansible"
    }

  }
  required_version = ">= 0.14"
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "default"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}
