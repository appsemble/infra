resource "helm_release" "appsemble" {
  name = var.appsemble_name
  repository = var.appsemble_repo
  chart = var.appsemble_chart
  namespace = var.appsemble_namespace
  create_namespace = true
  version = var.appsemble_version

# TODO refactor so that we supply an entire values.yaml file
  set {
    name = "version"
    value = var.appsemble_version
  }
  # set {
  #   name = "gitlab.app"
  #   value = var.appsemble_gitlab_app
  # }
  # set {
  #   name = "gitlab.env"
  #   value = var.appsemble_gitlab_env
  # }
  set {
    name = "ingress.enabled"
    value = var.appsemble_ingress_enabled
  }
  set {
    name = "ingress.host"
    value = var.appsemble_ingress_host
  }
  set {
    name = "forceProtocolHttps"
    value = true
  }
  set {
    name = "global.postgresql.auth.existingSecret"
    value = "postgresql-secret"
  }
  set {
    name = "global.postgresql.auth.database"
    value = var.appsemble_global_postgresql_auth_database
  }
  set {
    name = "global.postgresql.auth.username"
    value = var.appsemble_global_postgresql_auth_username
  }
      # --set "global.postgresql.auth.existingSecret=$POSTGRESQL_SECRET"
      # --set "global.postgresql.auth.database=$POSTGRESQL_DATABASE"
      # --set "global.postgresql.auth.username=$POSTGRESQL_USERNAME"
      # --set "global.postgresql.service.ports.postgresql=$POSTGRESQL_PORT"
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/proxy-body-size=50m"
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/force-ssl-redirect=\"true\""
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/ssl-redirect=\"true\""
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/hsts=\"true\""
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/hsts-include-subdomains=\"true\""
      # --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/hsts-max-age=\"31536000\""
      # --set "ingress.clusterIssuer=acme-issuer-prod"
      # --set "ingress.tls.secretName=appsemble-tls"
      # --set "ingress.tls.wildcardSecretName=appsemble-tls-wildcard"
      # --set "oauthSecret=oauth2"
      # --set "postgresql.fullnameOverride=$POSTGRESQL_HOST"
      # --set "postgresql.enabled=false"
      # --set 'postgresSSL=true'
      # --set "proxy=true"
      # --set "sentry.allowedDomains={*.appsemble.app, appsemble.app}"
      # --set "sentry.environment=production"
      # --set "sentry.secret=sentry"
      # --set "quotas.appEmail.enabled=true"
      # --set "quotas.appEmail.dailyLimit=10"
      # --set "quotas.appEmail.alertOrganizationOwner=true"
}
