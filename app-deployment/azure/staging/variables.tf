variable "appsemble_name" {
  type = string
  description = "The Appsemble release name"
}

variable "appsemble_repo" {
  type = string
  description = "The Appsemble Helm chart repository"
}

variable "appsemble_chart" {
  type = string
  description = "The Appsemble helm chart name"
}

variable "appsemble_namespace" {
  type = string
  description = "The Appsemble namespace name"
}

variable "appsemble_version" {
  type = string
  description = "The Appsemble Helm chart version"
}

# TODO process and understand meaning of below variables

variable "appsemble_gitlab_app" {
  type = string
  description = ""
}
variable "appsemble_gitlab_env" {
  type = string
  description = ""
}
variable "appsemble_global_postgresql_auth_database" {
  type = string
  description = ""
}
variable "appsemble_global_postgresql_auth_username" {
  type = string
  description = ""
}
variable "appsemble_global_postgresql_auth_existingSecret" {
  type = string
  description = ""
}
variable "appsemble_global_postgresql_service_ports_postgresql" {
  type = string
  description = ""
}
variable "appsemble_ingress_clusterIssuer" {
  type = string
  description = ""
}

variable "appsemble_ingress_enabled" {
  type = string
  description = ""
}
variable "appsemble_ingress_host" {
  type = string
  description = ""
}

variable "appsemble_ingress_tls_secretName" {
  type = string
  description = ""
}
variable "appsemble_ingress_tls_wildcardSecretName" {
  type = string
  description = ""
}

variable "appsemble_oauthSecret" {
  type = string
  description = ""
}

variable "appsemble_postgresql_fullnameOverride" {
  type = string
  description = ""
}
variable "appsemble_postgresql_enabled" {
  type = string
  description = ""
}
variable "appsemble_postgresSSL" {
  type = string
  description = ""
}

variable "appsemble_proxy" {
  type = string
  description = ""
}

variable "appsemble_sentry_allowedDomains" {
  type = string
  description = ""
}
variable "appsemble_sentry_environment" {
  type = string
  description = ""
}
variable "appsemble_sentry_secret" {
  type = string
  description = ""
}

variable "appsemble_quotas_appEmail_enabled" {
  type = string
  description = ""
}
variable "appsemble_quotas_appEmail_dailyLimit" {
  type = string
  description = ""
}
variable "appsemble_quotas_appEmail_alertOrganizationOwner" {
  type = string
  description = ""
}

