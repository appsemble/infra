# general
domain_name = "appsemble.nl"


# Namespace
ns_name = "appsemble-staging-ns"


# Container registry access
registry_server = "<registry server>"
registry_username = "<username of the container registry user>"
registry_password = "<password of the container registry user>"
registry_email = "<email of the container registry user>"


