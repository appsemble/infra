# Appsemble infra

## Requirements:

- `opentofu`
- `ansible`

## Parts:

- `cluster/azure/staging` - A cluster with a load balancer and a control plane
- `config/` - ansible recipes for setting up k3s and other services on the cluster
- `k8s-setup/` - provisions the cluster (In progress, translating https://gitlab.com/appsemble/infra/-/wikis/home to TF)
- `app-deployment/` - Helm deployment for Appsemble

Each folder has a `README.md` file with more information about it.

Each folder contains a set of terraform files, a `.tfvars.example`, and a `.terraform.lock.hcl` file.
Run `tofu init` in each folder to initialize the workspace with the necessary plugins.

Copy necessary `terraform.tfstate` files from ... (TODO: Where? dc/infra#102 File upload service) if not creating for the first time

### Apply changes

1. Check if everything is fine, you can do that without applying if you use:

```sh
tofu plan -var-file=.tfvars
```

2. Apply the configuration

```sh
tofu apply -var-file=.tfvars
```

You can destroy the current configuration using:

```sh
tofu destroy -var-file=.tfvars
```

## Variables

Variables can be found in BitWarden, if you have the necessary permissions.
Each part of the infrastructure has its own set of variables, so make sure to check the README.md in each folder.

## Docker

TODO
